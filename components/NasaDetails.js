import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { colors } from '../utils/index'
import { FontAwesome5, MaterialCommunityIcons } from '@expo/vector-icons'
import { useFonts } from 'expo-font';

const { PRIMARY_COLOR, SECONDARY_COLOR, BORDER_COLOR } = colors

export default function WeatherDetails({ data }) {

    const [loaded] = useFonts({
        ProximaLight: require('../assets/fonts/Proxima-Light.otf'),
        ProximaBold: require('../assets/fonts/Proxima-Bold.otf'),
    });

    if (!loaded) {
        return null;
    }

    return (
        <View style={styles.nasaDetails}>
            <View style={styles.nasaDetailsRow}>
                <View style={{ ...styles.nasaDetailsBox, borderRightWidth: 1, borderRightColor: BORDER_COLOR }}>
                    <View style={styles.nasaDetailsRow}>
                        <FontAwesome5 name="temperature-low" size={25} color={PRIMARY_COLOR} />
                        <View style={styles.nasaDetailsItems}>
                            <Text style={styles.textPrimary} >Equinox :</Text>
                            <Text style={styles.textSecondary}>{data.equinox}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.nasaDetailsBox}>
                    <View style={styles.nasaDetailsRow}>
                        <MaterialCommunityIcons name="water" size={30} color={PRIMARY_COLOR} />
                        <View style={styles.nasaDetailsItems}>
                            <Text style={styles.textPrimary} >Magnitude :</Text>
                            <Text style={styles.textSecondary}>{data.magnitude_h}</Text>
                        </View>
                    </View>
                </View>
            </View>
            <View style={{ ...styles.nasaDetailsRow, borderTopWidth: 1, borderTopColor: BORDER_COLOR }}>
                <View style={{ ...styles.nasaDetailsBox, borderRightWidth: 1, borderRightColor: BORDER_COLOR }}>
                    <View style={styles.nasaDetailsRow}>
                        <MaterialCommunityIcons name="weather-windy" size={30} color={PRIMARY_COLOR} />
                        <View style={styles.nasaDetailsItems}>
                            <Text style={styles.textPrimary}  >Orbital Id :</Text>
                            <Text style={styles.textSecondary}>{data.orbitId}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.nasaDetailsBox}>
                    <View style={styles.nasaDetailsRow}>
                        <MaterialCommunityIcons name="speedometer" size={30} color={PRIMARY_COLOR} />
                        <View style={styles.nasaDetailsItems}>
                            <Text style={styles.textPrimary} >Orbital Type :</Text>
                            <Text style={styles.textSecondary}>{data.orbitClassType}</Text>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    nasaDetails: {
        marginTop: 'auto',
        margin: 15,
        borderWidth: 1,
        borderColor: BORDER_COLOR,
        borderRadius: 10,
        backgroundColor: '#fff',
    },
    nasaDetailsRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    nasaDetailsBox: {
        flex: 1,
        padding: 20,
    },
    nasaDetailsItems: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    textSecondary: {
        fontSize: 18,
        color: SECONDARY_COLOR,
        fontWeight: '700',
        margin: 7,
    },
    textPrimary: {
        fontSize: 22,
        fontFamily: 'ProximaLight'
    }
})
