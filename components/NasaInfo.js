import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { colors } from '../utils/index'
import { useFonts } from 'expo-font';

const { PRIMARY_COLOR } = colors

export default function NasaInfo({ data }) {
    const [loaded] = useFonts({
        ProximaLight: require('../assets/fonts/Proxima-Light.otf'),
        ProximaBold: require('../assets/fonts/Proxima-Bold.otf'),
    });

    if (!loaded) {
        return null;
    }

    return (
        <View style={styles.nasaInfo}>
            <Image style={styles.nasaIcon} source={require('../assets/galaxy.png')} />
            <Text style={styles.name}>{data.id}</Text>
            <Text style={styles.textPrimary}>{data.designation}</Text>
            <Text style={styles.nasaDescription}>{data.orbitDesc}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    nasaInfo: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    nasaDescription: {
        fontSize: 18,
        textAlign: 'center',
        marginTop: 10,
        textTransform: 'capitalize',
        fontFamily: 'ProximaBold'
    },
    nasaIcon: {
        width: 100,
        height: 100,
    },
    textPrimary: {
        fontSize: 50,
        color: PRIMARY_COLOR,
        fontFamily: 'ProximaBold'
    },
    name: {
        fontFamily: 'ProximaBold',
        marginTop: 20,
        fontSize: 24,
    }
})
