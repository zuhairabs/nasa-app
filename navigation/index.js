import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
import InputPage from "../src/screens/InputPage";
import NasaPage from "../src/screens/NasaPage";

const Stack = createNativeStackNavigator();

function RootNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="InputPage" screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Home" component={InputPage} />
        <Stack.Screen name="Nasa" component={NasaPage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default RootNavigator