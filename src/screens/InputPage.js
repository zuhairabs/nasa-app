import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { Button, TextInput } from 'react-native-paper'
import { astroidIds } from '../../utils';

export default function InputPage({ navigation }) {
    const [asteroid, setAsteroid] = React.useState('');

    const handleSubmit = async () => {
        navigation.navigate('Nasa', { asteroid })
    }

    const randomId = () => {
        const id = astroidIds[Math.floor(Math.random() * astroidIds.length)]
        setAsteroid(id);
    }

    return (
        <View style={styles.container}>
            <Image style={styles.topIcon} source={require('../../assets/nasa-banner.png')} />
            <View style={styles.bottomContainer}>
                <TextInput style={styles.textInput} value={asteroid} label="Enter Asteroid ID" placeholder="Ex. 3729835" mode="outlined" onChangeText={asteroid => setAsteroid(asteroid)}/>
                <Button disabled={!asteroid || asteroid === " "} style={styles.textInput} mode="contained" onPress={handleSubmit}> Submit </Button>
                <Button style={styles.textInput} mode="contained" onPress={randomId}> Random Asteroid ID</Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#fbfcff',
    },
    textInput: {
        margin: 20,
    },
    button: {
        margin: 20,
    },
    topIcon: {
        flex: 1,
        width: 300,
        height: 300,
        marginTop: 100,
        alignSelf: 'center'
    },
    bottomContainer: {
        flex: 1,
    }
})
