import TestRenderer from 'react-test-renderer'
import { render } from "@testing-library/react-native"
import React from 'react'
import InputPage from './InputPage.js'

describe("<InputPage />", () => {
    it('renders correctly', () => {
        const tree = TestRenderer.create(<InputPage/>).toJSON();
        expect(tree).toMatchSnapshot();
    })
    it('renders default elements', () => {
        const { getAllByText } = render(<InputPage />)
        getAllByText('Enter Asteroid ID');
    })
})