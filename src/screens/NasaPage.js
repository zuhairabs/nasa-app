import { StatusBar } from 'expo-status-bar'
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, ActivityIndicator, Image } from 'react-native'
import NasaInfo from '../../components/NasaInfo'
import ReloadIcon from '../../components/ReloadIcon'
import NasaDetails from '../../components/NasaDetails'
import { colors } from '../../utils/index'
import { load } from '../../utils/loadApi';

export default function NasaPage({ route }) {
    const [errorMessage, setErrorMessage] = useState(null)
    const [data, setData] = useState(null)
    const [refresh, setRefresh] = useState(null)
    const { asteroid } = route.params

    useEffect( async() => {
        const { result, error } = await load(asteroid)
        if(error) {
            setErrorMessage(error);
        } else {
            setData(result);
        }
    }, [refresh])

    if (data) {
        return (
            <View style={styles.container}>
                <StatusBar style="auto" />
                <View style={styles.main}>
                    <ReloadIcon refresh={setRefresh} />
                    <NasaInfo data={data}/>
                </View>
                <NasaDetails data={data} />
            </View>
        )
    } else if (errorMessage) {
        return (
            <View style={styles.container}>
                <ReloadIcon refresh={setRefresh} />
                <Image style={styles.topIcon} source={require('../../assets/error.png')} />
                <Text style={{ textAlign: 'center', color: colors.PRIMARY_COLOR, padding: 20, fontWeight: 'bold', fontSize: 20 }}>{errorMessage}</Text>
                <StatusBar style="auto" />
            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <ActivityIndicator size="large" color={colors.PRIMARY_COLOR} />
                <StatusBar style="auto" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#fbfcff'
    },
    main: {
        justifyContent: 'center',
        flex: 1,
    },
    topIcon: {
        width: 400,
        height: 400,
        alignSelf: 'center'
    },
})