import TestRenderer from 'react-test-renderer'
import React from 'react'
import NasaPage from './NasaPage'

describe("<NasaPage />", () => {
    it('renders correctly', () => {
        const route = { params: { asteroid: {} } };
        const tree = TestRenderer.create(<NasaPage route={route} />).toJSON();
        expect(tree).toMatchSnapshot();
    })
})