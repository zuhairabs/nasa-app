export const colors = {
    PRIMARY_COLOR: 'rgba(102,23,175,255)',
    SECONDARY_COLOR: '#002651',
    BORDER_COLOR: '#dbdbdb',
}

export const astroidIds = ["3729835", "3542519", "3542520", "3542521", "3729836", "3729839", "3729840"]