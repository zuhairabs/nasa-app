const BASE_WEATHER_URL = 'https://api.nasa.gov/neo/rest/v1/neo'
const NASA_API_KEY = 'DEMO_KEY';

export async function load(astroidId) {
    try {
        const apiUrl = `${BASE_WEATHER_URL}/${astroidId}?api_key=${NASA_API_KEY}`
        const response = await fetch(apiUrl)
        const result = await response.json()
        const newResult = Object.assign({}, { id: result.id, designation: result.designation, magnitude_h: result.absolute_magnitude_h, orbitDesc: result.orbital_data.orbit_class.orbit_class_description, equinox: result.orbital_data.equinox, orbitId: result.orbital_data.orbit_id, orbitClassType: result.orbital_data.orbit_class.orbit_class_type })
        if (response.ok) {
            return { result: newResult, error: null}
        } else {
            return { result: null, error: result.message}
        }
    } catch (error) {
        return { result: null, error: error.message}
    }
}